#!/bin/bash

###################################################################################
#
# name        : tuduix_input.sh
# description : utilidad gráfica para entrar acciones a la bandeja de entrada
#               de tuduix. Hace uso de zenity
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_input.sh
#
###################################################################################

source /usr/local/bin/env.sh
source /usr/local/bin/env_inst.sh

input="$(zenity --entry --title "$TUDUIX_INPUT_TITLE" --text "$TUDUIX_INPUT_TEXT" | sed s/\*/\-/g)"

# Quitamos el posible * al principio (estará, por ejemplo, si hemos copiado
# una acción de someday, donde están en formato org-mode, que lleva un *
# (o más) principio de cada ítem
input=$(echo $input | sed s/\*/\-/g)


# Recopilamos mientras el usuario no pulse escape para salir
while [ "$input" != "" ]; do
	todo.sh -d $TUDUIX_HOME/todo.cfg add $input
	notify-send -a tuduix -t 3500 "Recopilado: $input"
	input=`zenity --entry --title "$TUDUIX_INPUT_TITLE" --text "$TUDUIX_INPUT_TEXT"`
done






