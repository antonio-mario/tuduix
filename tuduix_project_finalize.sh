#!/bin/bash

###################################################################################
#
# name        : tuduix_project_finalize.sh
# description : utilidad para finalizar un proyecto. Lo guarda en el histórico
#               de proyectos añadiendo la fecha de finalización en el nombre.
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_project_finalize.sh
#
###################################################################################

source /usr/local/bin/env.sh

date=`date +%Y-%m-%d--%H-%M-%S` 
script_name=`basename $0`
filename=`echo $SCRIPT_NAME | awk -F "." '{ print $1 }'`
log_file=/tmp/$filename-$date.log
err_file=/tmp/$filename-$date.err
num_params=$#

if [ $num_params -ne 1 ]; then
    echo ERROR. Uso: $script_name NOMBRE_PROYECTO
    exit 1
fi


project_name=$1
project_dir=$TUDUIX_PROJECTS_DIR/$project_name
next_actions_file=$project_dir/next_actions.txt
tuduix_temp_file=/tmp/tuduix_temp_file_$date
project_finalized_dir=$project_name-finalized-$date

project_exists=0
for project in $(ls $TUDUIX_PROJECTS_DIR); do
    if [ "$project" = "$project_name" ]; then
	project_exists=1
    fi
done

if [ $project_exists = 0 ]; then
    echo ERROR. El proyecto $project_name no existe
    exit 2
fi

# Si existen acciones del proyecto que queremos finalizar en $TUDUIX_FILE
# las movemos a next_actions de dicho proyecto
exists_in_todo=`cat $TUDUIX_FILE | grep $project_name | wc -l`
if [ $exists_in_todo -gt 0 ]; then
    # Hacemos un backup de $TUDUIX_FILE por si algo sale mal
    backup_tuduix_file.sh

    # Metemos las acciones siguientes activas en el fichero next_actions
    cat $TUDUIX_FILE | grep $project_name >> $next_actions_file

    # Pasamos a un fichero temporal todas las acciones de $TUDUIX_FILE menos
    # las del proyecto en cuestión
    cat $TUDUIX_FILE | grep -v $project_name >> $tuduix_temp_file

    # Finalmente, metemos dichas acciones (todas menos las del proyecto)
    # en el fichero $TUDUIX_FILE reemplazando lo que había
    cat $tuduix_temp_file > $TUDUIX_FILE
fi

# Nos quedamos con el número de acciones siguientes del proyecto
num_next_actions=`cat $next_actions_file | wc -l`

mv $TUDUIX_PROJECTS_DIR/$project_name $TUDUIX_PROJECTS_FINALIZED_DIR
mv $TUDUIX_PROJECTS_FINALIZED_DIR/$project_name $TUDUIX_PROJECTS_FINALIZED_DIR/$project_name-finalized_$date


echo
echo
echo -------------------------------------------
echo Proyecto finalizado: $project_name
echo -------------------------------------------

if [ $num_next_actions -ge 0 ]; then
    echo Con las siguientes acciones sin terminar:
    echo
    
    oldIFS=$IFS     # conserva el separador de campo
    IFS=$'\n'     # nuevo separador de campo, el caracter fin de línea
    
    count=0
    for line in $(cat $TUDUIX_PROJECTS_FINALIZED_DIR/$project_name-finalized_$date/next_actions.txt); do
	echo "- Acción $count: $line"
	((count++))
    done
    
    IFS=$old_IFS     # restablece el separador de campo predeterminado
else
    echo "No hay acciones siguientes sin terminar"
fi

