#!/bin/bash

###################################################################################
#
# name        : tuduix_process.sh
# description : realiza el procesado de la bandeja de entrada de tuduix
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_process.sh
#
###################################################################################

source /usr/local/bin/env.sh
#set -x
source /usr/local/bin/env_functions.sh

# Primero comprobamos si hay internet. Si no, no lo ejecutamos
# para evitar problemas de sincronización de los archivos
if [ $(check_internet.sh laptop) -ne 0 ]; then
    echo ERROR: No hay conexión a Internet. Saliendo...
    exit 2
fi

# Variables
# -----------------------------------
date_now=`date +%Y-%m-%d`
time_now=`date +%H:%M`
date_time_now=`date +%Y-%m-%d--%H-%M-%S`
backup_file=/tmp/backup_file_$date_time_now
procesado_temporal=/tmp/procesado_temporal_$date_time_now
file_temp=/tmp/file_temp_$date_time_now
colour_title=$COLOUR_RED
colour_inbox=$COLOUR_PURPLE


# Functions
# -----------------------------------
function process_trash () {
    echo -E $new_line >> $TUDUIX_TRASH_FILE
    notify-send -a tuduix -t 3500 "TRASH: $new_line"
}

function process_done () {
    echo -E x $date_time_now $new_line >> $TUDUIX_DONE_FILE
    notify-send -a tuduix -t 3500 "DONE: $new_line"
}

function process_someday () {
    echo "    * SOMEDAY  *     "
    echo "--------------------"
    count_someday_files=0
    for name_someday_file in $(ls $TUDUIX_SOMEDAY_DIR); do
	echo "$count_someday_files - $name_someday_file"
	count_someday_files=$((count_someday_files + 1))
    done
    read -e -p "Introduce índice del fichero de someday [otra tecla para cancelar]: " index_someday
    #    result=`echo $index_someday | grep "+" | wc -l`
    result=$(is_num $index_someday)
    if [ $result = "1" ]; then
	# Si es un número lo que hemos introducido, buscamos el fichero de someday 
	count_someday_files=0
	for z in $(ls $TUDUIX_SOMEDAY_DIR); do
	    if [ "$count_someday_files" = "$index_someday" ]; then
		file_someday=$z
	    fi
	    # echo "*****DEBUG***** z = $z - file_someday = $file_someday - i = $i  - j = $j - count = $count - someday = $someday -- $line"
	    # Incrementamos en 1 el contador
	    count_someday_files=$[$count_someday_files+1]
	done
	echo -E \* $date_time_now $new_line >> $TUDUIX_SOMEDAY_DIR/$file_someday
	notify-send -a tuduix -t 3500 "SOMEDAY: $new_line"
    else
	echo "----------------------------------------  Cancelado  ----------------------------------------"
	# Quitamos el :someday a la cadena
	new_line=$(echo $new_line | sed s/\:someday//g)
	echo -E $new_line >> $file_temp
	notify-send -a tuduix -t 3500 "Vuelve a INBOX: $new_line"
    fi
    
}

function process_waiting () {
    echo -E $date_time_now $new_line >> $TUDUIX_WAITING_FILE
    notify-send -a tuduix -t 3500 "WAITING: $new_line"
}

function process_calendar () {
    read -i "$date_now $time_now" -e -p "Introduce fecha y hora: " event_date
    read -i "$DEFAULT_EVENT_DURATION" -e -p "Introduce duración del evento: " event_duration
    read -e -p "Introduce descripción: " event_description
    # Quitamos la cadena :calendar de la tarea
    new_line=${new_line/":calendar"/}
    gcalcli --calendar $TUDUIX_CALENDAR_NAME add --title $new_line --when $event_date --duration $event_duration  --description event_description --reminder 10
    notify-send -a tuduix -t 3500 "CALENDAR: $new_line"
}

function process_tickler () {
    echo -E $new_line >> $TUDUIX_TICKLER_FILE
    notify-send -a tuduix -t 3500 "TICKLER: $new_line"
}

function process_project () {
    echo "    * PROYECTO *     "
    echo "--------------------"
    count_projects=0
    for z in $(ls $TUDUIX_PROJECTS_DIR); do
	echo "$count_projects - $z"
	count_projects=$((count_projects + 1))
    done
    read -e -p "Introduce índice del proyecto o nombre si es nuevo [c|C para cancelar]: " project
    # is_num devuelve 1 si el argumento es un número y 0 en caso contrario
    result=$(is_num $project)
    # Si es un número lo que hemos introducido, buscamos el proyecto
    if [ $result = "1" ]; then
	count_projects=0
	for z in $(ls $TUDUIX_PROJECTS_DIR); do
	    if [ "$count_projects" = "$project" ]; then
		project_selected_dir=$z
		is_inactive="`echo $z | grep $TUDUIX_PROJECTS_INACTIVE_PREFIX | wc -l`"
		# Si el proyecto está inactivo, le quitamos el prefijo porque no nos interesa guardar la acción en next_actions con dicho prefijo. Siempre
		# guardamos todas las acciones del proyecto sin prefijos, para poder activar y desactivar proyectos sin problemas y con menos procesamiento
		if [ "$is_inactive" -gt "0" ]; then
		    project_selected="`echo $z | awk -F $TUDUIX_PROJECTS_INACTIVE_PREFIX '{ print $2 }'`"
		else
		    # Si el proyecto no es inactivo no lleva prefijo, por lo que el nombre del proyecto es ése, tal cual
		    project_selected=$z
		fi
	    fi
	    # echo "*****DEBUG***** z = $z - prj = $prj - i = $i  - j = $j - count = $count - project = $project" 
	    count_projects=$[$count_projects+1]
	done
	read -i "$line " -e -p "Introduce texto acción siguiente: " new_line
	# Le concatenamos al principio el nombre del proyecto
	next_action=`echo $project_selected - $new_line`
	echo $next_action
	echo $project_selected_dir
	echo -E $next_action >> $TUDUIX_PROJECTS_DIR/$project_selected_dir/next_actions.txt
	notify-send -a tuduix -t 3500 "PROJECT: $next_action"
    elif [ $project = "c" ] || [ $project = "C" ]; then #Si pulsamos c|C cancelamos
	echo "----------------------------------------  Cancelado  ----------------------------------------"
	# Quitamos el :project a la cadena
	new_line=$(echo $new_line | sed s/\:project//g)
	echo -E $new_line >> $file_temp
	notify-send -a tuduix -t 3500 "Vuelve a INBOX: $new_line"
    else # Si hemos introducido un nombre es un proyecto nuevo
	# Nos quedamos con el primer carácter del nombre del proyecto que hemos introducido
	first_char=$(_get_first_char $project)
	# Si el primer carácter no es el signo "+", lo añadimos
	if [ $first_char != "+" ]; then
	    project="+"$project
	fi
	mkdir $TUDUIX_PROJECTS_DIR/$project
	read -i "$line " -e -p "Introduce texto acción siguiente: " new_line
	# Le concatenamos al principio el nombre del proyecto
	next_action=`echo $project - $new_line`
	echo -E $next_action >> $TUDUIX_PROJECTS_DIR/$project/next_actions.txt
	notify-send -a tuduix -t 3500 "PROJECT: $next_action"
    fi	
}

function process_next_action () {
    echo -E $new_line >> $file_temp
    notify-send -a tuduix -t 3500 "NEXT_ACTION: $new_line"
}


function process () {

    # Primer parámetro: fichero que se quiere procesar
    local input_file=$1
    
    # Contamos el número de elementos que hay en la
    # bandeja de entrada
    # NOTA: sed elimina las líneas en blanco (*$) y también las que sólo contienen
    #       espacios y tabs (\s)
    num_elem=$(cat $input_file | grep -v "@" | sed '/^\s*$/d' | wc -l)

    echo -e $colour_title $num_elem elementos en la bandeja de entrada $COLOUR_DEFAULT
    echo -e $colour_title         -------------------------------------- $COLOUR_DEFAULT

    if [ $num_elem -eq 0 ]; then
	echo "No hay nada que procesar. No hay elementos en la bandeja de entrada."
	exit 2
    fi
    echo $NONE

    # Sacamos las "cosas" de la bandeja de entrada
    # que son las que no tienen contexto
    cat $input_file | grep -v "@" >> $backup_file
    echo "----------------------------------------"
    echo "      LISTA DE ELEMENTOS EN INBOX"
    echo "----------------------------------------"
    cat $backup_file
    echo "----------------------------------------"
    echo -n "Pulsa intro para comenzar... "
    read s
    
    # echo mira el contenido de $backup_file
    # read a
    
    # Del fichero TUDUIX nos quedamos sólo con
    # las líneas que tienen contextos -quitamos las
    # "cosas" de la bandeja de entrada
    cat $input_file | grep "@" > $file_temp

    # echo mira el contenido de $file_temp
    # read a


    # # Para cada cosa de la bandeja de entrada pedimos
    # # que se edite y se procese adecuadamente
    # oldIFS=$IFS     # conserva el separador de campo
    # IFS=$'\n'     # nuevo separador de campo, el caracter fin de línea
    count_lines=0

    while IFS='' read -u 10 -r line
    do
	count_lines=$((count_lines + 1))
	echo -e "$colour_inbox"$count_lines de $num_elem - $line $COLOUR_DEFAULT
	read -i "$line " -e -p "Introduce texto tarea: " new_line
	trash=`echo $new_line | grep -i ":trash" | wc -l`
	done=`echo $new_line | grep -i ":done" | wc -l`
	someday=`echo $new_line | grep -i ":someday" | wc -l`
	waiting=`echo $new_line | grep -i ":waiting" | wc -l`
	calendar=`echo $new_line | grep -i ":calendar" | wc -l`
	tickler=`echo $new_line | grep -i "t:" | wc -l`
	project=`echo $new_line | grep -i ":project" | wc -l`
	
	if [ $trash -eq 1 ]; then
	    process_trash
	elif [ $done -eq 1 ]; then
	    process_done
	elif [ $someday -eq 1 ]; then
	    process_someday
	elif [ $waiting -eq 1 ]; then
	    process_waiting
	elif [ $calendar -eq 1 ]; then
	    process_calendar
	elif [ $tickler -eq 1 ]; then
	    process_tickler
	elif [ $project -eq 1 ]; then
	    process_project
	else
	    # Si no lleva ningún parámetro es una acción siguiente normal
	    process_next_action
	fi
	echo -e "\n"

    done 10< "$backup_file"

    IFS=$old_IFS     # restablece el separador de campo predeterminado
    # Si se han procesado todos los elementos (*TODOS*)
    if [ $count_lines = $num_elem ]; then
	cat $file_temp > $input_file
    else
	echo "Error al procesar"
    fi

    rm -f $backup_file 2>/dev/null
    rm -f $file_temp 2>/dev/null
}



[ -f VERSION-FILE ] && . VERSION-FILE || VERSION="1.2"
version() {
    cat <<-EndVersion
		todoix.sh $VERSION

		First release: 2017-06-20
		By: Antonio Mario Molina Saorín
		License: GPL, http://www.gnu.org/copyleft/gpl.html
	EndVersion
    exit 1
}

# Nos quedamos con el nombre del programa
tuduix_sh=$(basename "$0")
tuduix_full_sh="$0"
export TUDUIX_SH TUDUIX_FULL_SH

oneline_usage="$tuduix_sh"

usage()
{
    cat <<-EndUsage
		Usage: $oneline_usage
	EndUsage
    exit 1
}

shorthelp()
{
    cat <<-EndHelp
		  Usage: $oneline_usage
	EndHelp

    exit 0
}

help()
{
    cat <<-EndOptionsHelp
		  Usage: $oneline_usage
	EndOptionsHelp

    exit 1
}

die()
{
    echo "$*"
    exit 1
}

getPrefix()
{
    # Parameters:    $1: todo file; empty means $TUDUIX_FILE.
    # Returns:       Uppercase FILE prefix to be used in place of "TODO:" where
    #                a different todo file can be specified.
    local base=$(basename "${1:-$TUDUIX_FILE}")
    echo "${base%%.[^.]*}" | tr 'a-z' 'A-Z'
}


# Quitamos las líneas en blanco
/usr/local/bin/remove_blank_lines.sh $TUDUIX_FILE
process $TUDUIX_FILE

#set +x
