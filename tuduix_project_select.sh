#!/bin/bash

###################################################################################
#
# name        : tuduix_project_select.sh
# description : muestra la lista de todos los proyectos activos e inactivos.
#               Permite activar y desactivar proyectos. También permite finalizar
#               proyectos y editar las siguientes acciones de los mismos
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_project_select.sh
#
###################################################################################

source /usr/local/bin/env.sh

source /usr/local/bin/env_functions.sh

# Variables definition
# -----------------------------------
date_now=`date +%Y-%m-%d`
time_now=`date +%H:%M`
date_time_now=`date +%Y-%m-%d--%H-%M`
backup_file=/tmp/backup_file_$date_time_now
procesado_temporal=/tmp/procesado_temporal_$date_time_now
file_temp=/tmp/file_temp_$date_time_now
colour_title=$COLOUR_RED
colour_inbox=$COLOUR_YELLOW
cmd_editor=emacs
cmd_explorer=xdg-open

# Functions definition
# -----------------------------------
function project_select () {

    last_project=+nothing
    salir=0
    opcion_no_valida=0
    oldIFS=$IFS     # conserva el separador de campo
    IFS=$'\n'     # nuevo separador de campo, el caracter fin de línea
    while [ $salir = 0 ]; do
	
	clear
	count_projects=0
	for z in $(ls $TUDUIX_PROJECTS_DIR | grep -v "__OBJ__"); do

	    # Comprobamos si el proyecto es inactivo (si es 0 será activo y si es 1
	    # será inactivo)
	    project_inactive=$(echo $z | grep $TUDUIX_PROJECTS_INACTIVE_PREFIX |wc -l)

	    # Comprobamos el número de acciones siguientes
	    next_actions=$(cat $TUDUIX_PROJECTS_DIR/$z/next_actions.txt|wc -l)

	    # Vamos a comparar el nombre del proyecto de antes ($last_project) con el que estamos
	    # procesando pero sin tener en cuenta si está activo o si no lo está. Para ello, quitamos
	    # el prefijio $TUDUIX_PROJECTS_INACTIVE_PREFIX quedándonos con lo que hay después de "+"
	    project_name=`echo $z | awk -F "+" '{ print $2 }'`
	    last_project_name=`echo $last_project | awk -F "+" '{ print $2 }'`
	    
	    # Si el proyecto es el que hemos cambiado antes añadimos una cadena al final
	    # para distinguirlo visualmente
	    if [ $project_name = $last_project_name ]; then
		z=$z********************
	    fi

	    if [ $next_actions -eq 0 ]; then
		z=$z"_______NO_NEXT_ACTIONS"
	    fi
	    
	    # Si el proyecto es inactivo lo coloreamos del color COLOUR_PRJ_INACTIVE
	    if [ $project_inactive = 1 ]; then
		echo -e $COLOUR_PRJ_INACTIVE "$count_projects - $z" $COLOUR_DEFAULT
	    else # Si no, si el proyecto es activo, lo coloreamos con el color $COLOUR_PRJ_ACTIVE
		echo -e $COLOUR_PRJ_ACTIVE "$count_projects - $z" $COLOUR_DEFAULT
	    fi
	    ((count_projects++))
	done
	# Quitamos uno en el contador ya que siempre se aumenta uno demás en el bucle anterior
	((count_projects--))
	
	read -e -p "Selecciona número de proyecto a cambiar de estado (e|E delante para editar; f|F delante para finalizar; q/Q salir): " project

	# Nos quedamos con la primera palabra (por si se han introducido por error varias).
	# Por tanto, ignoramos todo lo demás detrás del primer espacio si lo hubiera.
	# Esto evita errores en las posteriores comprobaciones
	project=`echo $project | awk '{ print $1 }'`
	project_edit=`echo $project | grep -i e | wc -l`
	project_finish=`echo $project | grep -i f | wc -l`

	num_project=`is_num $project`
	
	# Si lo que hemos introducido NO es un número
	if [ $num_project = 0 ]; then
	    
	    if [ -z $project ]; then
		# Si la cadena está vacía (si hemos pulsado intro solamente) no hacemos nada,
		# con lo que volverá a mostrar la lista para que elijamos de nuevo
		:
	    elif [ $project = "q" ] || [ $project = "Q" ]; then
		# Si pulsa q/Q saldremos de la aplicación
		salir=1			
	    elif [ $project_edit = 1 ]; then
		# Si ha introducido eNUM es que quiere editar un proyecto, por lo que
		# lo abriremos con emacs y volveremos a mostrar la lista
		
		# Para ello, primero recorremos todos los proyectos en busca del seleccionado
		# el cuál lo obtenemos quitando la "e"
		project=`echo $project | sed 's/[e|E]//g'`
		count_projects=0
		for z in $(ls $TUDUIX_PROJECTS_DIR | grep -v "__OBJ__"); do
		    if [ "$count_projects" = "$project" ]; then
			project_selected=$z
		    fi
		    count_projects=$[$count_projects+1]
		done
		echo $project_selected | awk -F "+" '{ print "+" $2 }' | xclip -selection clipboard
		$cmd_editor $TUDUIX_PROJECTS_DIR/$project_selected/next_actions.txt 2>>/dev/null &
		$cmd_explorer $TUDUIX_PROJECTS_DIR/$project_selected
	    elif [ $project_finish = 1 ]; then
		# Si ha introducido fNUM es que quiere finalizar un proyecto, por lo que
		# ejecutaremos el comando para ello
		
		# Para ello, primero recorremos todos los proyectos en busca del seleccionado
		# el cuál lo obtenemos quitando la "f"
		project=$(echo $project | sed 's/[f|F]//g')
		count_projects=0
		for z in $(ls $TUDUIX_PROJECTS_DIR); do
		    if [ "$count_projects" = "$project" ]; then
			project_selected=$z
		    fi
		    count_projects=$[$count_projects+1]
		done

		notify-send -a tuduix -t 3500 "Proyecto $project_selected finalizado"
		tuduix_project_finalize.sh $project_selected
	    else
		# Si es cualquier otra cadena es una opción no válida
		opcion_no_valida=1
		
	    fi
	else # Si es un número lo que hemos introducido

	    # Si es un número, quitamos los primeros ceros, de forma que
	    # si ponemos 00009, nos quedamos con 9 solamente
	    
	    # Comprobamos si es un número de proyecto válido
	    if [ $project -ge 0 ] && [ $project -le $count_projects ]; then
		
		# Recorremos todos los proyectos en busca del seleccionado
		count_projects=0
		for z in $(ls $TUDUIX_PROJECTS_DIR); do
		    if [ "$count_projects" = "$project" ]; then
			project_selected=$z
		    fi
		    count_projects=$[$count_projects+1]
		done
		
		# Comprobamos si el proyecto es inactivo (si es 0 será activo y si es 1
		# será inactivo)
		project_inactive=`echo $project_selected | grep $TUDUIX_PROJECTS_INACTIVE_PREFIX|wc -l`
		
		# Si el proyecto es inactivo lo cambiamos a activo
		if [ $project_inactive = 1 ]; then
		    
		    # Quitamos el $TUDUIX_PROJECTS_INACTIVE_PREFIX del principio
		    project_active=`echo $project_selected | awk -F $TUDUIX_PROJECTS_INACTIVE_PREFIX '{ print $2 }'`
		    
		    # Renombramos el nombre del directorio
		    mv $TUDUIX_PROJECTS_DIR/$project_selected $TUDUIX_PROJECTS_DIR/$project_active

		else   # Si es un proyecto activo lo pasamos a inactivo

		    # Añadimos el $TUDUIX_PROJECTS_INACTIVE_PREFIX del principio
		    project_inactive=`echo $TUDUIX_PROJECTS_INACTIVE_PREFIX$project_selected`

		    # Comprobamos si el proyecto tiene acciones siguientes en $TUDUIX_FILE
		    has_next_actions=`cat $TUDUIX_FILE | grep $project_selected | wc -l`

		    # Si hay acción siguiente en $TUDUIX_FILE la metemos al fichero de next_actions
		    if [ $has_next_actions = 1 ]; then
			for action in $(cat $TUDUIX_FILE | grep $project_selected); do
			    echo "$action" > $file_temp

			    # DEBUG ********
			    # echo ACTION -------------- $action
			    # cat $file_temp
			    # echo pulsa intro 1
			    # read a
			    
			done
			cat $TUDUIX_PROJECTS_DIR/$project_selected/next_actions.txt >> $file_temp
			cat $file_temp > $TUDUIX_PROJECTS_DIR/$project_selected/next_actions.txt

			# DEBUG ********
			# echo pulsa intro 2
			# read b

			# También, sacamos dicha acción de $TUDUIX_FILE
			sed -i /$project_selected/d $TUDUIX_FILE
		    fi
		    
		    # Renombramos el nombre del directorio
		    mv $TUDUIX_PROJECTS_DIR/$project_selected $TUDUIX_PROJECTS_DIR/$project_inactive
		fi
		last_project=$project_selected
	    else
		opcion_no_valida=1
	    fi
	fi

	if [ $opcion_no_valida = 1 ]; then
	    read -e -p "Opción no válida. Pulse intro para continuar..." intro
	fi
    done
    IFS=$old_IFS     # restablece el separador de campo predeterminado
}



project_select
