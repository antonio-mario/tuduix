#!/bin/bash


###################################################################################
#
# name        : tuduix_tickler_process.sh
# description : procesa el archivo de seguimiento -tickler- en tuduix
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_tickler_process.sh
#
###################################################################################

source /usr/local/bin/env.sh

date=`date +%Y-%m-%d--%H-%M-%S`
today=`date +%Y-%m-%d`
tickler_temp=/tmp/tickler_temporal_$date
echo > $tickler_temp

# Para cada línea en el fichero de tickler (archivo de seguimiento) cuya
# fecha de activación (t:fecha) sea igual o menor  a la de hoy ($date) la metemos
# en el fichero $TUDUIX_FILE
oldIFS=$IFS     # conserva el separador de campo
IFS=$'\n'     # nuevo separador de campo, el caracter fin de línea

for i in $( cat $TUDUIX_TICKLER_FILE ); do
    task_date=`echo $i | awk -F "t:" '{ print $2 }' | awk '{ print $1 }'`
    
    if [[  "$task_date" < "$today" ]] || [[ "$task_date" == "$today" ]]; then
	echo $i >> $TUDUIX_FILE
	
    else
	echo $i >> $tickler_temp
    fi
done
IFS=$old_IFS     # restablece el separador de campo predeterminado

# Pasamos ese fichero temporal al fichero de tickler, es decir, al archivo
# de seguimiento
cat $tickler_temp > $TUDUIX_TICKLER_FILE




# Procesamos el fichero $TUDUIX_FILE y si hay alguna cadena t:YYYY-MM-DD,
# si es mayor que la fecha actual, movemos dicha entrada a TUDUIX_TICKLER_FILE
oldIFS=$IFS     # conserva el separador de campo
IFS=$'\n'       # nuevo separador de campo, el caracter fin de línea

# Archivo temporal para meter el contenido de $TUDUIX_FILE
tuduix_temp=/tmp/tuduix_temp_$date
touch $tuduix_temp

# Inicializamos el fichero $tickler_temp
echo > $tickler_temp
for line in $(cat $TUDUIX_FILE | grep -i "t:"); do

    # Nos quedamos con la fecha que hay a continuación de t:
    task_date=`echo $line | awk -F "t:" '{ print $2 }' | awk '{ print $1 }'`

    # Comparamos la fecha de la tarea con el día de hoy. Si es mayor que hoy
    # debe ir al fichero de tickler
    if [[ "$task_date" > "$today" ]]; then
	echo $line
	echo $line >> $TUDUIX_TICKLER_FILE
	echo $line >> $tickler_temp
    fi
done
IFS=$old_IFS     # restablece el separador de campo predeterminado

/usr/local/bin/remove_blank_lines.sh $tickler_temp

# Contamos el número de líneas que hemos metido en tickler (que son las
# que están en $tickler_temp)
tickler_lines=`cat $tickler_temp | wc -l`
cat $tickler_temp >> /tmp/1_tickler_temp
echo $tickler_lines >> /tmp/1_tickler_lines

# Si no hemos metido ninguna línea a tickler esta vez el fichero $tickler_temp
# estará vacío y, por cómo funciona "grep", si le pasamos a un "grep -v -f" un
# fichero vacío, el resultado será vacío, por lo que al final tendríamos un
# fichero $tuduix_temp vacío, con lo que, finalmente, tendríamos un fichero
# $TUDUIX_FILE vacío. Por tanto, sólo ejecutamos el "grep -v -f $tickler_temp"
# cuando precisamente $tickler_temp no sea vacío
if [ $tickler_lines -ne 0 ]; then
    echo dentro >> /tmp/1_tickler_lines
    cat $TUDUIX_FILE | grep -v -f $tickler_temp > $tuduix_temp
    cat $tuduix_temp > $TUDUIX_FILE
fi

/usr/local/bin/tuduix_clean_files.sh
