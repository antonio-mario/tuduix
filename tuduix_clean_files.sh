#!/bin/bash

###################################################################################
#
# name        : tuduix_clean_files.sh
# description : Elimina las líneas en blanco de los ficheros $TUDUIX_FILE y
#               $TUDUIX_TICKLER_FILE
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_clean_files.sh
#
###################################################################################

path="/usr/local/bin"

$path/remove_blank_lines.sh $TUDUIX_FILE
$path/remove_blank_lines.sh $TUDUIX_TICKLER_FILE
