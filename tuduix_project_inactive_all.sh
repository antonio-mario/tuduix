#!/bin/bash

###################################################################################
#
# name        : tuduix_project_inactive_all.sh
# description : inactiva todos los proyectos, lo que significa que le pone la 
#               cadenz "z__inactive" delante del nombre.
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_project_inactive_all.sh
#
###################################################################################

source /usr/local/bin/env.sh

# Con todos los proyectos activos (los que empiezan con +)
# los renombramos a z__inactive__+...
count=0
echo "Desactivando los proyectos:"
for i in $(find $TUDUIX_PROJECTS_DIR -maxdepth 1 -iname "+*"); do
	name=`basename $i`
	echo -e "\t $name --> z__inactive__$name"
	mv $TUDUIX_PROJECTS_DIR/$name $TUDUIX_PROJECTS_DIR/z__inactive__$name
	count=$((count + 1))
done

if [ $count = 0 ]; then
	echo "Ningún proyecto para desactivar"
fi

