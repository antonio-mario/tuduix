#!/bin/bash

###################################################################################
#
# name        : tuduix_project_create.sh
# description : crea el proyecto que se le pasa como argumento.
#               Es necesario introducir el "+" antes del nombre
#               (+project es la sintaxis de proyectoen todo.txt).
#               Nos solicita acciones siguientes para meter en el proyecto.
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_project_create.sh PROJECT
#
###################################################################################

source /usr/local/bin/env.sh

date=`date +%Y-%m-%d--%H-%M-%S` 
script_name=`basename $0`
filename=`echo $SCRIPT_NAME | awk -F "." '{ print $1 }'`
log_file=/tmp/$filename-$date.log
err_file=/tmp/$filename-$date.err
num_params=$#

if [ $num_params -ne 1 ]; then
    echo ERROR. Uso: $script_name NOMBRE_PROYECTO
    exit 1
fi

# El nombre del proyecto viene en el primer argumento
project_name="$1"

# Si no hay caracter indicador de proyecto (normalmente es el carácter +)
# lo añadimos
first_char=$(echo $project_name | cut -c 1)
if [ $first_char != $TUDUIX_PROJECT_PREFIX ]; then
    project_name="+$project_name"
fi
project_dir=$TUDUIX_PROJECTS_DIR/$project_name
next_actions_file=$project_dir/next_actions.txt

project_exists=0
for project in $(ls $TUDUIX_PROJECTS_DIR); do
	if [ "$project" = "$project_name" ]; then
		project_exists=1
	fi
done

if [ $project_exists = 1 ]; then
	echo ERROR. El proyecto $project_name ya existe
	exit 2
fi

mkdir -p $project_dir
touch $next_actions_file
next_action=""
read -p "Introduzca acción siguiente ("q/Q" para salir): " next_action

while [ "$next_action" != "q" ] && [ "$next_action" != "Q" ]; do
	echo "$project_name $next_action" >> $next_actions_file
	read -p "Introduzca acción siguiente ("q/Q" para salir): " next_action
done

echo
echo
echo -------------------------------------------
echo Proyecto creado: $project_name
echo -------------------------------------------
echo Acciones siguientes:
echo

oldIFS=$IFS     # conserva el separador de campo
IFS=$'\n'     # nuevo separador de campo, el caracter fin de línea

count=0
for line in $(cat $next_actions_file); do
	echo "- Acción $count: $line"
	((count++))
done

IFS=$old_IFS     # restablece el separador de campo predeterminado

