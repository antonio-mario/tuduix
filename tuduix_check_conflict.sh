#!/bin/bash

###################################################################################
#
# name        : tuduix_check_conflict.sh
# description : utilidad para comprobar si hay conflictos en el directorio de
#               tuduix. Los conflictos se dan si se tienen dos ficheros modificados
#               en distintos sitios sincronizados ambos con Dropbox. Lo que hace es
#               comprobar si hay algún fichero con la palabra "conflic" en él.
#               Como salida, devuelve la lista de ficheros en conclicto
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-09-22
# usage       : ./tuduix_check_conflict.sh
#
###################################################################################

source /usr/local/bin/env.sh

number_conflict_files=$(ls $TUDUIX_HOME/*conflic* 2>&1 | grep -iv "No " |  wc -l) 


if [ $number_conflict_files -gt 0 ]; then
    notify-send -a tuduix -t 3500 "FICHEROS EN CONFLICTO EN TODO_DIR"
    conflict_files=$(ls $TUDUIX_HOME/*conflic* 2>&1)
    echo $conflict_files
    exit 7
else
    exit 0
fi


