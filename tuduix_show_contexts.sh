#!/bin/bash

###################################################################################
#
# name        : tuduix_show_contexts.sh
# description : muestra los contextos que estamos usando en el archivo tuduix
#               (los contextos comienzan con "@")
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_show_contexts.sh
#
###################################################################################

source /usr/local/bin/env.sh

cat $TUDUIX_FILE | grep "@" | awk -F "@" '{ print $2 }' | awk '{  print $1 }' | sort | uniq 
