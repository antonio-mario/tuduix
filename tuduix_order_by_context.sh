#!/bin/bash

###################################################################################
#
# name        : tuduix_order_by_contexts.sh
# description : ordena el fichero tuduix por contextos
#               (los contextos comienzan con "@")
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2023-06-04
# usage       : ./tuduix_order_by_contexts.sh
#
###################################################################################

# Primero hacemos backup del fichero tuduix
/usr/local/bin/backup_tuduix_file.sh

source /usr/local/bin/env.sh
date=$(date +%Y-%m-%d--%H-%M-%S) 
file_tuduix_tmp="/tmp/tuduix_order_by_tmp_$date.txt"
file_tmp_1="/tmp/tuduix_tmp1_$date.txt"
file_tmp_2="/tmp/tuduix_tmp2_$date.txt"

for i in $(tuduix_show_contexts.sh); do
    cat $TUDUIX_FILE | grep "@$i"  >> $file_tuduix_tmp
done

# Comprobamos que tanto el ordenado como el original siguen teniendo las
# mismas tareas y no hay duplicados
sort $file_tuduix_tmp > $file_tmp_1
sort $TUDUIX_FILE > $file_tmp_2
diff $file_tmp_1 $file_tmp_2
salida=$?

# Si los ficheros están OK -tienen el mismo contenido- entonces
# entonces copiamos el contenido de tuduix pero ya ordenado en el fichero
# original de tuduix
if [ $salida -eq 0 ]; then
    cat $file_tuduix_tmp > $TUDUIX_FILE
else
    echo "CUIDADO, HAY ERRORES Y NO SE HA PODIDO ORDENAR"
fi



