#!/bin/bash

###################################################################################
#
# name        : env.sh
# description : carga las variables de entorno genéricas
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : source env.sh
#
###################################################################################


# ----------------------------------------------------------------------------------
# Lista de colores que admite bash
# ----------------------------------------------------------------------------------
export COLOUR_NONE=''
export COLOUR_BLACK='\033[0;30m'
export COLOUR_RED='\033[0;31m'
export COLOUR_GREEN='\033[0;32m'
export COLOUR_BROWN='\033[0;33m'
export COLOUR_BLUE='\033[0;34m'
export COLOUR_PURPLE='\033[0;35m'
export COLOUR_CYAN='\033[0;36m'
export COLOUR_LIGHT_GREY='\033[0;37m'
export COLOUR_DARK_GREY='\033[1;30m'
export COLOUR_LIGHT_RED='\033[1;31m'
export COLOUR_LIGHT_GREEN='\033[1;32m'
export COLOUR_YELLOW='\033[1;33m'
export COLOUR_LIGHT_BLUE='\033[1;34m'
export COLOUR_LIGHT_PURPLE='\033[1;35m'
export COLOUR_LIGHT_CYAN='\033[1;36m'
export COLOUR_WHITE='\033[1;37m'
export COLOUR_DEFAULT='\033[0m'

#----------------------------------------------------------------------------------
# Variables de entorno de constantes de cadenas y líneas
# ---------------------------------------------------------------------------------
export SEPARADOR_GUIONES="--------------------------------------------------------------------"
export STR_ERROR_CLOUD_STATUS="ERROR: El servicio de cloud no está ejecutándose. Abortando operación..."
export TERMINAL_WIDTH=90
export TERMINAL_HIGH=25


#----------------------------------------------------------------------------------
# Variables de entorno de estados de salida
# ---------------------------------------------------------------------------------
export ERROR_CLOUD_STATUS=11

#----------------------------------------------------------------------------------
# Variables de entorno relacionadas con rutas
# ---------------------------------------------------------------------------------
export FILES_CLOUD=/home/amms/Dropbox

#----------------------------------------------------------------------------------
# Variables de entorno relacionadas con tuduix
# ---------------------------------------------------------------------------------
export TUDUIX_HOME=$FILES_CLOUD/tuduix
export TUDUIX_PERSPECTIVE=$TUDUIX_HOME/perspectiva
export TUDUIX_FILE=$TUDUIX_HOME/tuduix.txt
export TUDUIX_TRASH_FILE=$TUDUIX_HOME/trash.txt
export TUDUIX_DONE_FILE=$TUDUIX_HOME/done.txt
export TUDUIX_TICKLER_FILE=$TUDUIX_HOME/tickler.txt
export TUDUIX_WAITING_FILE=$TUDUIX_HOME/waiting.txt
export TUDUIX_SOMEDAY_DIR=$TUDUIX_HOME/someday
export TUDUIX_PROJECTS_DIR=$TUDUIX_HOME/projects
export TUDUIX_PROJECTS_INACTIVE_PREFIX="z__inactive__"
export TUDUIX_PROJECTS_ACTIVE_DIR=$TUDUIX_HOME/projects/active_projects
export TUDUIX_PROJECTS_INACTIVE_DIR=$TUDUIX_HOME/projects/inactive_projects
export TUDUIX_HISTORIC_FILES=$TUDUIX_HOME/0__historico_tuduix
export TUDUIX_PROJECTS_FINALIZED_DIR=$TUDUIX_HISTORIC_FILES/0__finalized_projects
export TUDUIX_PROJECTS_TEMPLATES_DIR=$TUDUIX_HOME/0__templates_projects
export TUDUIX_PROJECTS_TEMPLATES_SUFFIX="XXX"
export TUDUIX_INBOX_DIR=/mnt/datos/mis_archivos/Drive/0__inbox
export TUDUIX_ROUTINES=$TUDUIX_HOME/rutinas.txt
export TUDUIX_ROUTINES_FIELD_SEPARATOR="|"
export TUDUIX_PROJECT_PREFIX="+"
export DEFAULT_EVENT_DURATION=5
export COLOUR_PRJ_ACTIVE=$COLOUR_LIGHT_GREEN
export COLOUR_PRJ_INACTIVE=$COLOUR_LIGHT_RED
export TUDUIX_INPUT_TITLE="Tuduix"
export TUDUIX_INPUT_TEXT="Introduzca texto a recopilar:"
export TUDUIX_LABEL_TIME=":tiempo"
export TUDUIX_LABEL_ENERGY=":energía"
export TUDUIX_LABEL_PRIORITY=":prioridad"
export TUDUIX_MAX_HOURS_A_WEEK=8
export TUDUIX_CURRE_CONTEXTS="@pro_112 @þro_groca"


#----------------------------------------------------------------------------------
# Variables de entorno generales
# ----------------------------------------------------------------------------------
export XMODIFIERS=""



