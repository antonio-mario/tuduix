#!/bin/bash

###################################################################################
#
# name        : tuduix_routine_process.sh
# description : procesa la lista de rutinas y mete en tuduix las que coinciden
#               en la fecha
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_routine_process.sh
#
###################################################################################

source /usr/local/bin/env.sh
source /usr/local/bin/env_functions.sh


cloud_started=$(ps -ef | grep "insync start" | grep -v grep | wc -l)

# Si el programa de la nube (para sincronización de los archivos) NO está actualizado SALIMOS
if [ $cloud_started -ne 1 ]; then
	 echo $STR_ERROR_CLOUD_STATUS
	 exit $ERROR_CLOUD_STATUS
fi

date=`date +%Y-%m-%d--%H-%M-%S`
today_date=`date +%Y-%m-%d`
today_yn=`date +%Y`
today_yn_is_even=`is_even $today_yn`
today_mn=`date +%m`
today_mn_is_even=`is_even $today_mn`
today_md=`date +%d`
today_md_is_even=`is_even $today_mf`
today_wn=`date +%W`
today_wn_is_even=`is_even $today_wn`
today_wd=`date +%u`
today_wd_is_even=`is_even $today_wd`
routine_temp=/tmp/routine_temporal_$date
cat $TUDUIX_ROUTINES | grep -v "#" > $routine_temp

# Para cada línea en el fichero del archivo de rutinas semanales
# si el día coincide con el de hoy o bien pone "all" o bien coincide
# la paridad entonces lo metemos al fichero temporal (que luego se pasará al
# fichero $TUDUIX_FILE

oldIFS=$IFS     # conserva el separador de campo
IFS=$'\n'     # nuevo separador de campo, el caracter fin de línea


for i in $( cat $routine_temp ); do
	# El último awk es para quedarnos con la primera palabra solamente
	# y de paso nos quitamos todos los espacios
	yn=`echo $i | awk -F "$TUDUIX_ROUTINES_FIELD_SEPARATOR" '{ print $1 }' | awk '{ print $1 }'`
	mn=`echo $i | awk -F "$TUDUIX_ROUTINES_FIELD_SEPARATOR" '{ print $2 }' | awk '{ print $1 }'`
	md=`echo $i | awk -F "$TUDUIX_ROUTINES_FIELD_SEPARATOR" '{ print $3 }' | awk '{ print $1 }'`
	wn=`echo $i | awk -F "$TUDUIX_ROUTINES_FIELD_SEPARATOR" '{ print $4 }' | awk '{ print $1 }'`
	wd=`echo $i | awk -F "$TUDUIX_ROUTINES_FIELD_SEPARATOR" '{ print $5 }' | awk '{ print $1 }'`
	task=`echo $i | awk -F "$TUDUIX_ROUTINES_FIELD_SEPARATOR" '{ print $6 }'`

	echo ----------------------------------------
	echo "yn - $yn ** mn - $mn ** md - $md ** wn - $wn ** wd - $wd ** $task"
	
	# Inicializamos variable -se pondrá a uno si hay coincidencia de la fecha con la de la rutina
	
	# Comparamos el año

	# Inicializamos variable
	found_yn=0
	case "$yn" in
		# impar
		odd)
			# Si el actual es par no hacemos nada
			if [ $today_yn_is_even -eq 0 ]; then
				 echo No coincide año
			else
				 echo Año impar
				 found_yn=1
			fi
			;;
		# par
		even)
			# Si el actual es par ponemos a 1 la variable "found"
			if [ $today_yn_is_even -eq 0 ]; then
				 found_yn=1
				 echo Año impar
			else
				 echo Año par
			fi
			;;
		# todos
		all)
			 # Si pone "all" ponemos "found" a 1
			 echo Año all
			 found_yn=1
			;;
		# otra cosa (normalmente el número concreto)
		*)
			# Si es el año específico de la rutina
			if [ "$yn" = "$today_yn" ]; then
				 found_yn=1
				 echo Año coincide exactamente
			else
				 echo Error Año
			fi
			;;
	esac

	if [ $found_yn -eq 1 ]; then
		
		# Comparamos el número de mes

		# Inicializamos variable
		found_mn=0
		case "$mn" in
			# impar
			odd)
				# Si el actual es par no hacemos nada
				 if [ $today_mn_is_even -eq 0 ]; then
					  echo mn -  $mn
					  echo $today_mn_is_even
					  echo $found_mn
					  echo odd
					 echo No coincide mes
				else
					 found_mn=1
					 echo Mes impar encontrado
				fi
				;;
			# par
			even)
				# Si el actual es par ponemos a 1 la variable "found"
				 if [ $today_mn_is_even -eq 0 ]; then
					echo Mes par encontrado
					found_mn=1
				 else
					  echo Mes impart encontrado
				 fi
					  
				;;
			# todos
			all)
				 # Si pone "all" ponemos "found" a 1
				 echo Mes all
				found_mn=1
				;;
			# otra cosa (normalmente el número concreto)
			*)
				# Si es el año específico de la rutina
				if [ "$mn" = "$today_mn" ]; then
					 found_mn=1
					 echo mes coincide exactamente
				else
					 echo Error mes
				fi
				;;
		esac
	fi
		
		
	# Comparamos el día del mes

	# Inicializamos variable
	found_md=0
	if [ $found_yn -eq 1 ] && [ $found_mn -eq 1 ]; then
		case "$md" in
			# impar
			odd)
				# Si el actual es par no hacemos nada
				if [ $today_md_is_even -eq 0 ]; then
					 echo No coincide día del mes
				else
					 found_md=1
					 echo Día impar encontrado
				fi
				;;
			# par
			even)
				# Si el actual es par ponemos a 1 la variable "found"
				if [ $today_md_is_even -eq 0 ]; then
					 found_md=1
					 echo Día par encontrado
				else
					 echo Día impar, no coincide
				fi
				;;
			# todos
			all)
				# Si pone "all" ponemos "found" a 1
				 found_md=1
				 echo Día all
				;;
			# otra cosa (normalmente el número concreto)
			*)
				 echo md - $md
				 echo today_md - $today_md
				# Si es el año específico de la rutina
				if [ "$md" -eq "$today_md" ]; then
					 found_md=1
					 echo Día coincide exactamente
				else
					 echo Error día
				fi
				;;
		esac
	fi

	# Comparamos el número de semana

	# Inicializamos variable
	found_wn=0
	if [ $found_yn -eq 1 ] && [ $found_mn -eq 1 ] && [ $found_md -eq 1 ]; then
		case "$wn" in
			# impar
			odd)
				# Si el actual es impar no hacemos nada
				if [ $today_wn_is_even -eq 0 ]; then
					 echo "No coincide semana"
				fi
				;;
			# par
			even)
				# Si el actual es par ponemos a 1 la variable "found"
				if [ $today_wn_is_even -eq 0 ]; then
					found_wn=1
				fi
				;;
			# todos
			all)
				# Si pone "all" ponemos "found" a 1
				found_wn=1
				;;
			# otra cosa (normalmente el número concreto)
			*)
				# Si es el año específico de la rutina
				if [ "$wn" = "$today_wn" ]; then
					found_md=1
				fi
				;;
		esac
	fi


	# Comparamos el día de la semana
	
	# Inicializamos variable
	found_wd=0
	if [ $found_yn -eq 1 ] && [ $found_mn -eq 1 ] && [ $found_md -eq 1 ] &&  [ $found_wn -eq 1 ]; then
		case "$wd" in
			# impar
			odd)
				# Si el actual es impar no hacemos nada
				if [ $today_wd_is_even -eq 0 ]; then
					echo No conicide día de la semana
				fi
				;;
			# par
			even)
				# Si el actual es par ponemos a 1 la variable "found"
				if [ $today_wd_is_even -eq 0 ]; then
					found_wd=1
				fi
				;;
			# todos
			all)
				# Si pone "all" ponemos "found" a 1
				found_wd=1
				;;
			# otra cosa (normalmente el número concreto)
			*)
				
				# Si es el año específico de la rutina
				if [ "$wd" = "$today_wd" ]; then
					found_wd=1
				fi
				;;
		esac
	fi

	
	# Si está a 1 la variable found_wd, que es la última en comparar es que todas las
	# anteriores han sido 1 también, por lo que no es necesario especificar que las demás
	# deben estar a 1 -porque lo estarán seguro si ésta está a 1-
	if [ $found_wd -eq 1 ]; then
		is_in_todo=`cat $TUDUIX_FILE | grep $task |wc -l`
		# Si no está en $TUDUIX_FILE lo metemos
		if [ $is_in_todo -eq 0 ]; then
			echo $task metida en fichero TODO
			echo $task >> $TUDUIX_FILE
		fi
	fi
#	echo $yn __ $found_yn ----- $mn __ $found_mn ----- $md __ $found_md ----- $wn __ $found_wn ----- $wd __$found_wd
done

