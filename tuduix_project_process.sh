#!/bin/bash

###################################################################################
#
# name        : tuduix_project_process.sh
# description : revisa la lista de acciones siguientes de tuduix y las listas
#               de acciones siguientes de los proyectos activos. Mete la siguiente
#               acción de cada proyecto activo si no hay ninguna en tuduix.txt
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_project_process.sh
#
###################################################################################

source /usr/local/bin/env.sh

DATE=`date +%Y-%m-%d--%H-%M` 
next_actions_project_temporal=/tmp/next_actions_project_temporal_$DATE
tuduix_temporal=/tmp/tuduix_temporal_$DATE
projects_lost=/tmp/proyectos_perdidos_$DATE
projects_without_next_actions=/tmp/proyectos_sin_acciones_siguientes_$DATE

# Para cada proyecto activo (quitamos los inactivos, que son los
# que llevan la palabra "inactive" en su nombre de directorio)
# comprobamos si hay acción siguiente. Si no, metemos una de la lista
# de acciones siguientes del proyecto (fichero next_actions.txt)

count_projects_without_next_actions=0

for i in $(ls $TUDUIX_PROJECTS_DIR | grep -i -v inactive ); do

    
    # Contamos las acciones siguientes del proyecto
    has_actions=$(cat $TUDUIX_PROJECTS_DIR/$i/next_actions.txt | wc -l)

    # Comprobamos si hay acciones siguientes del proyecto en $TUDUIX_FILE
    exists_in_todo=$(cat $TUDUIX_FILE | grep -i $i | wc -l)

    # Comprobamos si hay acciones siguientes del proyecto en $TICKLER_FILE
    exists_in_tickler=$(cat $TUDUIX_TICKLER_FILE | grep -i $i | wc -l)

    # Si el proyecto no aparece en la lista de acciones siguientes,
    # es decir, si no hay ninguna acción siguiente ni en la lista
    # de acciones siguientes ni en el tickler...
    if [ $exists_in_todo = 0 ] && [ $exists_in_tickler = 0 ]; then

	# Sólo si el proyecto tiene acciones siguientes haremos esto
	if [ $has_actions -ne 0 ]; then
	    
	    # Cogemos la primera línea de acciones siguientes del proyecto
	    next_action=`head -1 $TUDUIX_PROJECTS_DIR/$i/next_actions.txt`
	    # Metemos la línea en el fichero de lista de acciones siguientes
	    echo $next_action >> $TUDUIX_FILE

	    # Eliminamos dicha acción de la lista de acciones siguientes del proyecto
	    cat $TUDUIX_PROJECTS_DIR/$i/next_actions.txt | tail -n +2 > $next_actions_project_temporal
	    cat $next_actions_project_temporal > $TUDUIX_PROJECTS_DIR/$i/next_actions.txt

	    # Si la acción lleva "parallel-XX" es que puede que haya alguna
	    # acción más que se puede realizar en paralelo, por lo que tenemos
	    # que recorrer las acciones del proyecto para coger todas las
	    # que llevan el mismo "parallel-XX"

	    num_parallels=`echo $next_action | grep "parallel" | wc -l`
	    if [ $num_parallels -ne 0 ]; then
		parallel_string=`echo $next_action | grep -o '\b\parallel-[a-zA-Z0-9]*\b'`

		cat $TUDUIX_PROJECTS_DIR/$i/next_actions.txt
		while IFS= read -r linea; do
		    is_parallel=`echo $linea | grep $parallel_string |wc -l`
		    if [ $is_parallel -ne 0 ]; then
			echo "$linea" >> $TUDUIX_FILE
		    fi
		done < $TUDUIX_PROJECTS_DIR/$i/next_actions.txt

		# Eliminamos todas las acciones en paralelo de la lista de next_actions del proyecto
		# pues ya las hemos metido en TODO
		cat $TUDUIX_PROJECTS_DIR/$i/next_actions.txt | grep -v $parallel_string > $next_actions_project_temporal
		cat $next_actions_project_temporal > $TUDUIX_PROJECTS_DIR/$i/next_actions.txt					
	    fi
	else
	    echo $i >> $projects_without_next_actions
	    ((count_projects_without_next_actions++))
	fi
    fi
done


# Para cada proyecto inactivo (que son los
# que llevan la palabra "inactive" en su nombre de directorio)
# quitamos la siguiente acción de tuduix.txt y la metemos en su archivo
# next_actions correspondiente
for i in $(ls $TUDUIX_PROJECTS_DIR | grep -i inactive ); do

    # Nos quedamos con el nombre del proyecto -sin el "inactive" inicial
    project=`echo $i | awk -F "z__inactive__" '{ print $2 }'`

    # Guardamos las acciones siguientes de ese proyecto en un fichero temporal
    cat $TUDUIX_FILE | grep $project > $next_actions_project_temporal
    
    # Metemos dichas acciones siguientes de next_actions a continuación de éstas
    # en el fichero temporal -ya que las de TUDUIX_FILE son las primeras-
    cat $TUDUIX_PROJECTS_DIR/$i/next_actions.txt >> $next_actions_project_temporal

    # Metemos todas las acciones siguientes del fichero temporal -que son el conjunto
    # total de acciones del proyecto y en orden- en el fichero next_actions.txt,
    # ignorando lo que hubiera antes (que son las acciones que ya hemos metido al
    # temporal)
    cat $next_actions_project_temporal > $TUDUIX_PROJECTS_DIR/$i/next_actions.txt

    # Metemos en un fichero temporal todo lo que hay en $TUDUIX_FILE menos las acciones
    # del proyecto que está inactivo
    cat $TUDUIX_FILE | grep -v $project > $tuduix_temporal
    
    # El fichero temporal ahora es el que tiene las acciones válidas (es decir,
    # todas las que había en $TUDUIX_FILE menos las del proyecto inactivo
    cat $tuduix_temporal > $TUDUIX_FILE

done




# Recorremos todo el fichero $TUDUIX_FILE y vamos recorriendo todas acciones que
# pertenecen a proyectos, que son las que tienen el nombre del proyecto en la misma acción y,
# por tanto, todas llevan el carácter "+" (que es el primero de un nombre de proyecto)

oldIFS=$IFS     # conserva el separador de campo
IFS=$'\n'     # nuevo separador de campo, el caracter fin de línea
echo $DATE >> $projects_lost
count_projects_lost=0
for i in $( cat $TUDUIX_FILE | grep "+" | grep -v " + " ); do

    # Nos quedamos sólo con el nombre del proyecto
    project_name=$(echo $i | awk -F "+" '{ print $2 }' | awk '{ print $1 }')

    # Comprobamos si el proyecto de $TUDUIX_FILE aparece o no en la lista
    # de proyectos activos
    same_project=$(ls $TUDUIX_PROJECTS_DIR | grep -i $project_name | wc -l)

    # Si no hay coincidencia entre la tarea de $TUDUIX_FILE y ninguno
    # de los proyectos (tanto activos como inactivos)
    if [ $same_project = 0 ]; then
	
	# Este fichero es donde guardaremos todos los proyectos que aparecen en $TUDUIX_FILE
	# pero que no están en $TUDUIX_PROJECTS_DIR
	echo $i >> $projects_lost
	
	# Aumentamos el contador de proyectos perdidos (queremos saber cuántos hay)
	((count_projects_lost++))
    fi
    

done

IFS=$old_IFS     # restablece el separador de campo predeterminaod
# echo $count_projects_lost lost
if [ $count_projects_lost != 0 ]; then
    echo "LISTA PROYECTOS QUE NO TIENEN DIRECTORIO ASOCIADO"
    echo "-----------------------------------------------------"
    cat $projects_lost | grep +
fi

echo ""
echo ""

# echo $count_projects_without_next_actions projects_without
if [ $count_projects_without_next_actions != 0 ]; then
    echo "LISTA PROYECTOS QUE NO TIENEN ACCIÓN SIGUIENTE EN $TUDUIX_FILE"
    echo "----------------------------------------------------------------"
    cat $projects_without_next_actions
fi

rm -rf $projects_without_next_actions
rm -rf $projects_lost





