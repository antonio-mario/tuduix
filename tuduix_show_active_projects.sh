#!/bin/bash

###################################################################################
#
# name        : tuduix_show_active_projects.sh
# description : muestra la lista de proyectos activos
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_show_active_projects.sh
#
###################################################################################

source /usr/local/bin/env.sh

count=1
for i in $(ls $TUDUIX_PROJECTS_DIR | grep -i -v inactive); do
	echo "$count - $i"
	((count++))
done

((count--))
exit $count
