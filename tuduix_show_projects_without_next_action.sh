#!/bin/bash

###################################################################################
#
# name        : tuduix_show_projects_without_next_action.sh
# description : muestra los projectos que no tienen siguiente acción
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_show_projects_without_next_action.sh
#
###################################################################################

source /usr/local/bin/env.sh

echo Projectos que no tienen acción siguiente:
echo ----------------------------------------

count=0
# Para cada projecto activo buscamos acción siguiente en todo.txt
for i in $(ls $TUDUIX_PROJECTS_DIR | grep -v inactive | grep -v __OBJ__); do
	 # Buscamos si tiene alguna acción siguiente en el fichero TUDUIX_FILE
	 next_actions=$(cat $TUDUIX_FILE | grep $i | wc -l)
	 
	 # Buscamos si tiene alguna acción siguiente en el fichero next_actions del
	 # directorio del proyecto en cuestión
	 next_actions_in_file=$(cat $TUDUIX_PROJECTS_DIR/$i/next_actions.txt | grep $i | wc -l)

	 # Si no hay ninguna acción siguiente en ninguno de los dos sitios
	 if [ $next_actions -eq 0 ] && [ $next_actions_in_file -eq 0 ]; then
		  ((count++))
		  echo "  $count - $i"
		  notify-send -a tuduix -t 3500 "  $count - $i"
	 fi
	 
done

exit $count
