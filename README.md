# tuduix: gestión de los TODO en linux 
## Introducción
**tuduix:** sistema de scripts bash para la gestión de las tareas pendientes en un fichero de texto plano que usa un formato similar al que plantea [todo.txt](http://todo.txt). 

El nombre viene de "TUDÚ" In linuX, es decir, TO-DO in Linux, o lo que es lo mismo, una herramienta para gestionar nuestras tareas, nuestras acciones pendientes, en un entorno Linux (1)

La finalidad de tuduix es permitirte gestionar los "TODO", es decir, las cosas que tenemos que hacer, de una forma lo más completa posible. 

## Instalación
Descarga o clona el repositorio. Consiste en una serie de scripts, cada uno con una finalidad concreta que abajo explicamos. Cópialos/muévelos o simplemente haz un enlace simbólico a /usr/local/bin.

Requerimientos: 
- notify_send (para mostrar notificaciones en el escritorio).
- sed_file.sh (script para cambiar una cadena por otra dentro de un fichero).

Una vez instalado hay que modificar el fichero env.sh, donde se declaran las distintas variables de entorno y ajustarlas a nuestro gusto.

## Fichero de texto plano. Formato

Todas las acciones siguientes se guardan en un fichero de texto plano llamado "tuduix.txt" y que sigue el formato, o más bien deberíamos decir, la convención del sistema "todo.txt".

El formato original de "todo.txt" plantea, entre otros, lo siguiente:

- cada acción es una línea (una secuencia de caracteres hasta el primer intro)
- cada proyecto es una palabra que empieza por el carácter "+"
- cada contexto es una palabra que empieza por el carácter "@"

En nuestro caso, el fichero se llama "tuduix.txt" y además permite estas etiquetas:

- *:tiempo N* para especificar el tiempo previsto que se tardará en hacer la tarea
- *:energia N* para especificar la energía que necesitaremos para llevarla a cabo

Una acción asociada a un **proyecto** es simplemente una acción que contiene (da igual el lugar) una cadena con el nombre del proyecto (precedido, por supuesto, por el signo "+", que es el que lo identifica como proyecto).

Una acción sin cadena de proyecto es una acción unitaria independiente.

Todas las acciones siguientes tienen un **contexto** asignado (se lo asignaremos durante el procesado -ver script *tuduix_process.sh* más abajo-).

Las acciones sin contexto son las que conforma la **bandeja de entrada**, es decir, las "cosas" que anotamos para despejar la mente y tomar nota de ellas para un posterior procesado que indicará qué son exactamente y qué haremos con ellas (en caso de ser necesario realizar algo, se incluirá una acción siguiente añadiendo el contexto adecuado).

El fichero *tuduix.txt* contendrá siempre todas las acciones de todos los contextos (estén o no asociadas a un proyecto). También contendrá las acciones del contexto **@waiting**, es decir, "a la espera", que son las acciones que tenemos delegadas.

Como vemos, estamos usando conceptos que son transversales en muchas metodologías de productividad, como GTD©. Precisamente, **tuduix** nace para poder realizar todo el procesado y gestión de proyectos necesario para un sistema de productividad más completo.

## Qué tipo de programa es **tuduix**

**tuduix** es un conjunto de scripts bash que permiten implementar una metodología más completa de gestión de las acciones siguientes. En concreto, consta de los siguientes scripts:

- env.sh            

Definición de variables de entorno necesarias. Aquí se definen todos los archivos y directorios que se usan en la aplicación. Simplemente cambiándolos aquí ya lo tendréis todo configurado para que los scripts funcionen correctamente (no tendréis que modificar nada en ningún script).

- env_functions.sh  

Definición de funciones genéricas de bash

- tuduix_input.sh                  

Muestra una ventana para introducir acciones siguientes. Podemos introducir tantas como queramos. Se cierra con ESCAPE.

- tuduix_process.sh

Realiza el procesado de las acciones de la bandeja de entrada, es decir, las líneas del archivo que NO contienen un contexto.

- tuduix_project_create.sh

Nos permite crear un proyecto cuyo nombre pasaremos como argumento. Tras esto, nos permite meter acciones siguientes del proyecto.

- tuduix_project_finalize.sh

Nos permite finalizar un proyecto (esté o no activo -hablaremos de gestión de proyectos más abajo). 

- tuduix_project_inactive_all.sh

Pasa a inactivos todos los proyectos. Esto es útil en cada revisión semanal, pues nos permite *resetear* la lista de proyectos para elegir (**activar**) sólo aquellos en los que queremos trabajar la próxima semana.

- tuduix_project_process.sh

Revisa la lista de acciones siguientes de *tuduix.txt* y las listas de acciones siguientes de cada proyecto activo (veremos más adelante en qué consiste esto realmente) de forma que si no hay acciones siguientes del proyecto en *tuduix.txt* meterá la siguiente acción de ese proyecto (eliminándola, lógicamente, de la lista de acciones siguientes de dicho proyecto).

- tuduix_project_select.sh

Un interfaz en modo texto que nos muestra la lista de proyectos actuales y donde podemos activar, desactivar, finalizar y editar los mismos.

- tuduix_project_to_inactive.sh

Convierte un proyecto en inactivo.

- tuduix_routine_process.sh

Gestiona las rutinas, es decir, las tareas periódicas. Cada vez que lo ejecutamos, recorre el archivo "rutinas.txt" en busca de aquéllas que se deben activar hoy (por ser día par/impar, por coincidir la semana, o si es semana par o impar, o si coincide el mes...). En caso de coincidir, añade dicha acción a la lista de acciones siguientes.

- tuduix_show_active_projects.sh

Muestra sólo los proyectos activos.

- tuduix_show_contexts.sh

Muestra los contextos que estamos usando en nuestro fichero "tuduix.txt".

- tuduix_show_inactive_projects.sh

Muestra los proyectos inactivos.

- tuduix_show_projects.sh

Muestra todos los proyectos.

- tuduix_show_projects_without_next_action.sh

Muestra todos los proyectos que no tienen acción siguiente. Esto nos avisa de que tenemos que revisar dicho proyecto y pensar cuál es la siguiente acción (o acciones) siguiente(s) que tenemos que llevar a cabo para que el proyecto avance.

- tuduix_tickler_process.sh

Gestiona el *tickler* o *archivo de seguimiento*. Esto es un fichero (*tickler.txt*) que contiene aquellas acciones que deben activarse a llegar a una fecha en concreto.

## Gestión de proyectos

La meta de **tuduix** es poder realizar una implementación completa de una herramienta de gestión de la productividad bajo Linux y sobre texto plano, de la forma más sencilla posible (siguiendo el [principio KISS](https://es.wikipedia.org/wiki/Principio_KISS)).

En el caso de los proyectos, entendiendo por tales aquellos resultados que nos llevarán más de una acción para completarlos, simplemente tenemos un directorio llamado **projects** donde tendremos un subdirectorio por proyecto. El nombre del subdirectorio será precisamente el nombre del proyecto (incluyendo el carácter identificacivo "+"). 

De esta forma, nuestra de proyectos será un simple listado del directorio *projects*.

Dentro de cada directorio tenemos **siempre** un fichero llamado *next_actions.txt*. Este fichero incluirá cada una de las acciones siguientes necesarias para completar el proyecto y en su orden adecuado.

Además de este fichero, podremos guardar ahí (ya opcionalmente) todo el material asociado al proyecto, de forma que siempre lo tengamos en su sitio, en el propio proyecto.

### Proyectos activos y proyectos inactivos

Los proyectos activos son aquellos en los que estamos trabajando ahora. De esta forma, el script *tuduix_project_process.sh*, encargado de meter la siguiente acción de cada proyecto cada vez que completemos la actual, mirará sólo los proyectos activos.

Los inactivos no los tendremos en cuenta a la hora del trabajo de esta semana. Será en la siguiente revisión semanal cuando los miremos todos (con el script *tuduix_project_select.sh*) y decidamos cuáles queremos activar y cuáles no para la siguiente semana.

Un proyecto activo no tiene nada especial que lo identifique como tal, por lo que cualquier proyecto recién creado es, por defecto, un proyecto activo.

Si queremos que un proyecto activo pase a ser inactivo, simplemente le añadiremos al nombre la cadena "z__inactive__", de forma que si tenemos un proyecto activo llamado *+declaracion_de_la_renta_presentada* y lo queremos pasar a inactivo, lo renombramos a *z__inactive__+declaracion_de_la_renta_presentada*. El poner la "z" delante es por legibilidad, porque al hacer un *ls* en el directorio de proyectos, los inactivos quedarán al final.

De todas formas, tenemos un script que convierte un proyecto en inactivo (*tuduix_project_to_inactive.sh*).

### Finalización de proyectos

Cuando un proyecto se finaliza simplemente se mueve al directorio 0__historico_tuduix/0__finalized_projects. Todas estas rutas y archivos se pueden cambiar de nombre y poner el que más nos guste. Los que indico son los nombres que vienen por defecto. Todo esto se configura en el fichero "env.sh", el cuál cargan todos los scripts al principio, y que podéis personalizar sin problemas.

## La nube...

Con esto que hemos comentado ya podéis usar esta aplicación en cualquier PC pero, si queréis usarla desde varios PCs, el archivo *tuduix.txt* y todos los demás archivos y ficheros necesarios, tendrán que estar en un sitio accesible desde todos ellos. Se puede usar cualquier sistema basado en la nube, como Dropbox (éste es el que yo uso, en su versión gratuita, más que suficiente para esta aplicación). 

También podéis usar otra plataforma en la nube e incluso una que os montéis vosotros. Lo importante es, si queréis usar la aplicación desde varios equipos distintos, que todos tenga acceso a los archivos en todo momento para que todos actualicen los cambios sin problemas y no haya conflictos.

## Uso desde el móvil/tablet

Vale, con esto podemos gestionar nuestros pendientes en Linux pero, ¿y en el móvil? Nuestro fichero de acciones siguientes es un simple texto plano, que podríamos editar desde cualquier editor sin problemas, pero para que sea más cómodo y rápido, sobre todo si se usa desde el móvil, lo suyo es usar una aplicación diseñada para ello.

Os voy a enumerar 2 que existen para la plataforma Android (también están para la plataforma Apple, pero yo no la uso por lo que no puedo aportar información al respecto). Básicamente son la aplicación *Todo.txt*, que se llama así precisamente, y también tenemos *Simpletask*,  que es la que yo uso actualmente. En este último caso tenéis la versión para *Dropbox* y la versión *Nextcloud*. También está la versión *cloudless* pero en este caso el archivo estaría en local en el móvil y no podríais usarlo desde *tuduix*.

## Rutinas (tareas periódicas)

Existe un fichero llamado *rutinas.txt* que contiene la lista de tareas que se activarán cuando la fecha actual coincida con la fecha para la que se ha programado la rutina. Por este motivo, es conveniente que este script esté en el *crontab* para que se ejecute como mínimo una vez al día.

El formato de este fichero sería así:

(YN)       |    (MN)    |   (MD)    |    (WN)    |   (WD)   |    Texto de la tarea

Serían líneas de texto cuyos campos están separados por el signo "|", siendo el último campo el texto de la tarea en sí. En cada uno de los campos (excepto el de la tarea, donde escribiremos el texto de la misma) seguiremos un formato concreto. Veamos qué significa cada campo y qué formato usaríamos en él:

### YN: Year Number.
Descripción: año. Ejemplo: 2020, 2021, etc.
Formato: (odd|even|2017|2018|...|all)

### MN: Month Number.
Descripción: número de mes. Rango: 1..12
Formato: (odd|even|1|2|...|12|all) 

### MD: Month Day.
Descripción: número de día del mes. Rango: 1..31
Formato: (odd|even|1|2|3...|31|all)

### WN: Week Number.
Descripción: número de semana del año. Rango: 1..52.
Formato:  (odd|even|1|2|...|52|all) | Texto de la tarea

### WD: Week Day.
Descripción: día de la semana. Rango: 1..7
Formato: (odd|even|1|2|...|7|all)

### Texto de la tarea
Descripción: aquí pondremos el texto de la tarea.
Formato: ninguno en particular. Simplemente, que debería llevar incorporado el contexto. Si no lo lleva deberemos procesar esta línea cuando procesemos el inbox completo. Esto puede ser conveniente si es algo que queremos definir más concretamente justo en el momento en que se active esta acción.

---
(1)  o basado en Unix, por lo que también podríamos usarlo en Mac OS; con las últimas mejoras introducidas en powershell también es posible ejecutarlos en Windows, aunque yo me referiré expresamente a su uso bajo Linux, que es el sistema que uso.
