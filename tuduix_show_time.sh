#!/bin/bash


###################################################################################
#
# name        : tuduix_show_time.sh
# description : utilidad para mostrar el tiempo que ocupa cada contexto en la lista 
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : ./tuduix_show_time.sh
#
###################################################################################

source /usr/local/bin/env.sh
source /usr/local/bin/env_inst.sh

date=`date +%Y-%m-%d--%H-%M-%S` 
script_name=`basename $0`
filename=`echo $SCRIPT_NAME | awk -F "." '{ print $1 }'`
log_file=/tmp/$filename-$date.log
err_file=/tmp/$filename-$date.err
tmp_file=/tmp/$filename-$date.txt
num_params=$#

if [ $num_params -eq 1 ]; then
	cat $TUDUIX_FILE | grep $1 > $tmp_file
elif [ $num_params -eq 0 ]; then
	cat $TUDUIX_FILE > $tmp_file
else
	echo ERROR. Uso: $script_name [CONTEXT]
    exit 1
fi

echo $SEPARADOR_GUIONES

# Calculamos las acciones por contexto
oldIFS=$IFS     # conserva el separador de campo
IFS=$'\n'     # nuevo separador de campo, el caracter fin de línea
total=0
total_curre=0
contexts=$(tuduix_show_contexts.sh)
for i in $contexts; do
	for j in $(cat $tmp_file | grep $i); do
		has_time_label=`echo $j | grep $TUDUIX_LABEL_TIME | wc -l`
		if [ $has_time_label -ne 0 ]; then
			t=`echo $j | awk -F $TUDUIX_LABEL_TIME '{ print $2 }' | awk '{ print $1 }'`
			total=$(($total+$t))

			# Nos quedamos con el contexto de la línea (tarea) que estamos procesando
			context=`echo $j| awk -F "@" '{ print $2 }' | awk '{ print $1 }'`
			context_in_curre=`echo $TUDUIX_CURRE_CONTEXTS | grep $context | wc -l`
			if [ $context_in_curre = 1 ]; then
				total_curre=$(($total_curre+$t))
			fi
		fi
	done
	time_in_hours=`minutes_to_hours.sh $total`
	echo "contexto $i : $total minutos --> $time_in_hours"
	total=0
done

echo $SEPARADOR_GUIONES

# Calculamos el total de minutos de todas las acciones independientemente
# del contexto
for j in $(cat $tmp_file); do
	has_time_label=`echo $j | grep $TUDUIX_LABEL_TIME | wc -l`
	if [ $has_time_label -ne 0 ]; then
		t=`echo $j | awk -F $TUDUIX_LABEL_TIME '{ print $2 }' | awk '{ print $1 }'`
		total=$(($total+$t))
	fi
done

echo
total_curre_in_hours=`minutes_to_hours.sh $total_curre`
echo "Minutos de curre: $total_curre --> $total_curre_in_hours"
echo $SEPARADOR_GUIONES

IFS=$old_IFS     # restablece el separador de campo predeterminado



hours=`echo $total_curre_in_hours | awk -F ":" '{ print $1 }'`

if [ $num_params -eq 0 ]; then
	if [ $hours -lt $TUDUIX_MAX_HOURS_A_WEEK ]; then
		dif=$(($TUDUIX_MAX_HOURS_A_WEEK-$hours))
		echo Aún restan $dif horas para poder trabajar en tus objetivos y tareas
	elif [ $hours -gt $TUDUIX_MAX_HOURS_A_WEEK ]; then
		dif=$(($hours-$TUDUIX_MAX_HOURS_A_WEEK))
		echo Sobran $dif horas. Has sido demasiado optimista. Quita tareas o no te dará tiempo a todo
	else
		echo Si te lo propones podrás realizar todas esas tareas. Hay tiempo. Ánimo!
	fi
fi

echo $SEPARADOR_GUIONES
time_in_hours=`minutes_to_hours.sh $total`
echo "Total: $total minutos --> $time_in_hours"	

echo $SEPARADOR_GUIONES
