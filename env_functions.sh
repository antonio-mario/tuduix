#!/bin/bash

###################################################################################
#
# name        : env_functions.sh
# description : carga las funciones de uso genérico
# version     : 1.0
# author      : antonio-mario (http://antonio-mario.com)
# date        : 2020-04-24
# usage       : source env_functions.sh
#
###################################################################################

# ----------------------------------------------------------------------------------
# Funciones genéricas
# ----------------------------------------------------------------------------------
is_num() {
    awk -v a="$1" 'BEGIN {print (a == a + 0)}';
}

is_even() {

    echo $((10#$1%2))


    # if ($1 % 2)) ; then
    #    # Si el resto cuando divides por 2 es distinto de 1 devolvemos 1,
    #    # es decir, que es impar
    #    echo 1
    # else
    #    # Si da 0 devolvemos 0, es decir, es par
    #    echo 0
    # fi	
}


function _get_file_extension() {
    ext=$(echo $1 | awk -F "." '{ print $NF }')
    echo "$ext"
}

function _file_uncompress() {
    # $1 fichero a descomprimir
    # $2 dir destino donde descomprimir
    ext=$(_get_file_extension $1)
    case "$ext" in
	"tgz")
	    tar zxvf "$1" -C "$2"
	    ;;
	"zip")
	    unzip "$1" -d "$2"
	    ;;
	"rar")
	    unrar x "$1" "$2"
	    ;;
	*)
	    #		echo "Unknown file extension $1"
	    ;;
    esac
}

function _difference_dates() {

    segundos=""
    minutos=""
    horas=""
    dias=""
    
    date_mayor=$1
    date_menor=$2
    
    segundos=$(($date_mayor - $date_menor))

    # Si el resultado es mayor o igual a 3600 segundos, calculamos los minutos
    if [ $segundos -ge 60 ]; then
	minutos=$(($segundos/60))
	segundos=$(($segundos%60))
    fi

    # Si hay más de 60 minutos calculamos las horas
    if [ $minutos -ge 60 ]; then
	horas=$(($minutos/60))
	minutos=$(($minutos%60))
    fi

    # Si hay más de 24 horas calculamos los días
    if  [ $horas -ge 24 ]; then
	dias=$(($horas/24))
	horas=$(($horas%24))
    fi

    if [ -n $dias ]; then
	echo "$dias días, $horas horas, $minutos minutos, $segundos segundos"
    elif [ -n $horas ]; then
	echo "$horas horas, $minutos minutos, $segundos segundos"
    elif [ -n $minutos ]; then
	echo "$minutos minutos, $segundos segundos"
    elif [ -n $segundos ]; then
	echo "$segundos segundos"
    else
	echo "0"
    fi
}

function _get_first_char() {
    # Devuelve el primer carácter de la cadena pasada como argumento
    first_char=$(expr substr $1 1 1)

    echo $first_char
}
